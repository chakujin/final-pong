/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [Cristian Galindo]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, EPICTITLE,SORPRESA, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    Color TitleColor = YELLOW;
    //TitleColor.a = 0;
    int TitleSize = 0;
     bool fadeOut = true;
    float alpha = 0;
    float fadeSpeed = 0.01f;
    
    Color recColor = BLACK;
    //Rectangle rec = {screenWidth/3, screenHeight/3, 300, 200};

    
    int framesCounter = 0;
    const int velocidady = 8;
    const int velocidadIay = 6;
    const int minVelocity = 5;
    const int maxVelocity = 5;
    const int ballSize = 15;
   
   Vector2 ball;
    ball.x = screenWidth/2;
    ball.y = screenHeight/2;
    
    Vector2 ballVelocity;
    ballVelocity.x = minVelocity;
    ballVelocity.y = minVelocity;
     
    Rectangle fondo;
   
    fondo.width = 800;
    fondo.height = 450;    
    fondo.x = 0;
    fondo.y = 0;
   
   Rectangle palaizquierda;
   
    palaizquierda.width = 20;
    palaizquierda.height = 100;    
    palaizquierda.x = 50;
    palaizquierda.y = screenHeight/2 - palaizquierda.height/2;
    
    Rectangle paladerecha;
   
    paladerecha.width = 20;
    paladerecha.height = 100;    
    paladerecha.x = screenWidth - 50 - paladerecha.width;
    paladerecha.y = screenHeight/2 - paladerecha.height/2;
    
    Rectangle life1;
   
    life1.width = 90;
    life1.height = 20; 
    life1.x = 0;
    life1.y = 30;
    
    Rectangle life2;
   
    life2.width = 90;
    life2.height = 20; 
    life2.x = screenWidth - life1.width;
    life2.y = 30;
    
    int iaLinex = screenWidth/2;
    int timeCounter = 99;
    bool pause = false;
    int score1p = 0;
    int score2p = 0;
    // NOTE: Here there are some useful variables (should be initialized)
   /* Rectangle player;
    int playerSpeedY;
    
    Rectangle enemy;
    int enemySpeedY;
    
    Vector2 ballPosition;
    Vector2 ballSpeed;
    int ballRadius ;
    
    int playerLife;
    int enemyLife;
    
    int secondsCounter = 99;
    
    int framesCounter = 0;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined */
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    InitAudioDevice(); 
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont va*riables here (after InitWindow)
    Texture2D texture = LoadTexture("recursos/bandera.png");
    Texture2D dinero = LoadTexture("recursos/dinerito.png");
    Texture2D trump = LoadTexture("recursos/memetrump.png");
    Music music = LoadMusicStream("recursos/urss.ogg");
    Music valkirias = LoadMusicStream("recursos/valkirias.ogg");
    Music memesong = LoadMusicStream("recursos/memesong.ogg");
    Music gamemusic = LoadMusicStream("recursos/game.ogg");
    Music musicaeeuu = LoadMusicStream("recursos/eeuu.ogg");
    Sound choque = LoadSound("recursos/choque.wav"); 
    Sound point = LoadSound("recursos/point.wav");
    Sound aliens = LoadSound("recursos/aliens.wav");
    Sound fast = LoadSound("recursos/fast.wav"); 
    Sound fastdos = LoadSound("recursos/fastdos.wav");
    Sound lose = LoadSound("recursos/lose.wav");   
    Sound win = LoadSound("recursos/win.wav");     
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                framesCounter++;
               
                if(fadeOut)
                {
                    alpha += fadeSpeed;
                   
                    if(alpha >= 1.0f)
                    {
                        alpha = 1.0f;
                    }
                }
                else
                {
                    alpha -= fadeSpeed;
                    if(alpha <= 0.0f)
                    {
                        alpha = 0.0f;
                    }
                }
                if(framesCounter > 210)
                {
                    fadeOut = false;
                }
                if (framesCounter > 300)
                {
                    screen = TITLE;
                    framesCounter = 0;
                }
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                
            } break;
            
            case TITLE: 
            {
                
                // Update TITLE screen data here!
                PlayMusicStream(music);
                UpdateMusicStream(music);
                // TODO: Title animation logic.......................(0.5p)
                //if (TitleColor.a < 255) TitleColor.a++;
                if (TitleSize<60){
                    TitleSize++;   
                }
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                if (IsKeyPressed(KEY_ENTER)) screen = GAMEPLAY;
                if (IsKeyPressed(KEY_F)) screen = EPICTITLE;
                framesCounter++;
            } break;
            case EPICTITLE: 
            {
                // Update TITLE screen data here!
                PlayMusicStream(musicaeeuu);
                UpdateMusicStream(musicaeeuu);
                // TODO: Title animation logic.......................(0.5p)
                //if (TitleColor.a < 255) TitleColor.a++;
                if (TitleSize<60){
                    TitleSize++;   
                }
                if (IsKeyPressed(KEY_F)) screen = TITLE;
                
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                if (IsKeyPressed(KEY_ENTER)) screen = SORPRESA;
               
                framesCounter++;
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!
            if(!pause){
                PlayMusicStream(gamemusic);
                UpdateMusicStream(gamemusic);
            }
                // TODO: Ball movement logic.........................(0.2p)
                if(!pause){
            ball.x += ballVelocity.x;
            ball.y += ballVelocity.y;
        }
        if (timeCounter == 90){
            PlaySound(fast);
        }
        if (timeCounter == 70){
            PlaySound(fastdos);
        }
        if(timeCounter >= 70 && timeCounter<90){
           ball.x += ballVelocity.x;
            ball.y += ballVelocity.y;
            ballVelocity.x == 6;
            ballVelocity.y == 6;
            velocidadIay == 16;
        }

        if(timeCounter >= 30 && timeCounter<70){
                    ball.x += ballVelocity.x;
                    ball.y += ballVelocity.y;
                    ballVelocity.x == 6;
                    ballVelocity.y == 6;
                    velocidadIay == 16;
                    palaizquierda.height = 60;
                    
                }
       if(timeCounter >=0 && timeCounter<30){
                   ball.x += ballVelocity.x;
                    ball.y += ballVelocity.y;
                    ballVelocity.x == 10;
                    ballVelocity.y == 10;
                    velocidadIay == 18;
                }
                // TODO: Player movement logic.......................(0.2p)
                if(!pause){
            if (IsKeyDown(KEY_Q)){
              palaizquierda.y -= velocidady;
            }
            
            if (IsKeyDown(KEY_A)){
              palaizquierda.y += velocidady;
            }
          }
          if(palaizquierda.y<0){
            palaizquierda.y = 0;
        }
       
        if(palaizquierda.y > (screenHeight - palaizquierda.height)){
            palaizquierda.y = screenHeight - palaizquierda.height;
        }
           if(ball.x > screenWidth - ballSize){
            PlaySound(point);
            score1p++;
            ball.x = screenWidth/2;
            ball.y = screenHeight/2;
            ballVelocity.x = -minVelocity;
            ballVelocity.y = minVelocity;
            
        }
                // TODO: Enemy movement logic (IA)...................(1p)
        if (!pause){       
            
        if( ball.x > iaLinex){
                    if(ball.y > paladerecha.y){
                        paladerecha.y+=velocidadIay;
                    }
                    
                    if(ball.y < paladerecha.y){
                        paladerecha.y-=velocidadIay;
                    }
                }       
        }
        
        
       if(paladerecha.y<0){
            paladerecha.y = 0;
        }
       
        if(paladerecha.y > (screenHeight - paladerecha.height)){
            paladerecha.y = screenHeight - paladerecha.height;
        }
       
        
        else if(ball.x < ballSize){
            PlaySound(point);
            score2p++;
            ball.x = screenWidth/2;
            ball.y = screenHeight/2;
            ballVelocity.x = minVelocity;
            ballVelocity.y = minVelocity;
        }
                // TODO: Collision detection (ball-player) logic.....(0.5p)

        if(CheckCollisionCircleRec(ball, ballSize, palaizquierda)){
            PlaySound(choque);      
            if(ballVelocity.x<0){
                if(abs(ballVelocity.x)<maxVelocity){
                    ballVelocity.x *=-1;
                    ballVelocity.y *= 1;
                }else{
                    ballVelocity.x *=-1;
                }
            }
        }
        

                // TODO: Collision detection (ball-enemy) logic......(0.5p)
        if(CheckCollisionCircleRec(ball, ballSize, paladerecha)){
               PlaySound(choque);
               if(ballVelocity.x>0){
                    if(abs(ballVelocity.x)<maxVelocity){
                        ballVelocity.x *=-1;
                        ballVelocity.y *= 1;
                    }else{
                    ballVelocity.x *=-1;
                }
            }
        }
                // TODO: Collision detection (ball-limits) logic.....(1p)
            if((ball.y > screenHeight - ballSize) || (ball.y < ballSize) ){
            ballVelocity.y *=-1;
        }

                // TODO: Life bars decrease logic....................(1p)
                if(score1p == 1){
                    life2.width=60;
                }
                if(score1p == 2){
                    life2.width=30;
                }
                if(score1p == 3){
                    PlaySound(lose);
                    life2.width=0;
                }
                if(score2p == 1){
                    life1.width=60;
                }
                if(score2p == 2){
                    life1.width=30;
                }
                if(score2p == 3){
                    PlaySound(win);
                    life1.width=0;
                }
                
                if(score1p  == 3 || score2p == 3){
                    screen = ENDING;
                }
                // TODO: Time counter logic..........................(0.2p)

              if (!pause) {
                   framesCounter++;
                if (framesCounter > 60)
                {
                    timeCounter--;
                    framesCounter = 0; 
                }
               

                if (timeCounter < 0) screen = ENDING;
              }
                // TODO: Game ending logic...........................(0.2p)
                
                // TODO: Pause button logic..........................(0.2p)
                if (IsKeyPressed(KEY_P)){
            pause = !pause;
                }
            } break;
            case SORPRESA: 
            {
                // Update END screen data here!
                PlayMusicStream(memesong);
                UpdateMusicStream(memesong);
                if (IsKeyPressed(KEY_R)){
                    return 0;
                }
                // TODO: Replay / Exit game logic....................(0.5p)
                
            } break;           

           case ENDING: 
            {
                // Update END screen data here!
                if (IsKeyPressed(KEY_R)){
                    return 0;
                }
                if(timeCounter == 0){
                   PlaySound(aliens);
                }
                if(score2p == 3){
                    PlayMusicStream(memesong);
                    UpdateMusicStream(memesong); 
                    }
                if(score1p == 3){
                    PlayMusicStream(valkirias);
                    UpdateMusicStream(valkirias);
                    }
                // TODO: Replay / Exit game logic....................(0.5p)
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    //DrawRectangleRec(rec, Fade(recColor, alpha));
                    DrawText("DESIGNED BY STALIN", screenWidth/2 - MeasureText("DESIGNED BY STALIN", 40)/2, screenHeight/2, 40, Fade(recColor, alpha));
                    
                    // TODO: Draw Logo...............................(0.2p)
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                     DrawTexture(texture, screenWidth/2 - texture.width/2, screenHeight/2 - texture.height/2, WHITE);
                     DrawText("COMMUNIST PONG", screenWidth/2 - MeasureText("COMMUNIST PONG", 60)/2, screenHeight/2, TitleSize, TitleColor);  
                     
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    if ((framesCounter/30)%2) DrawText("PRESS ENTER", 270, 300, 30, YELLOW);
                    DrawText("PRESS F IF YOU ARE NOT COMMUNIST", 500, 40, 10, YELLOW);
                } break;
                case EPICTITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                     DrawTexture(dinero, screenWidth/2 - texture.width/2, screenHeight/2 - texture.height/2, WHITE);
                     DrawTexture(trump, screenWidth/2, 200, WHITE);
                     DrawText("CAPITALIST PONG", screenWidth/2 - MeasureText("CAPITALIST PONG", 60)/2, screenHeight/2, TitleSize, BLACK);  
                     DrawText("PRESS F IF YOU ARE COMMUNIST", 500, 40, 10, BLACK);
                     DrawText("DESIGNED BY DONALD TRUMP",400,80, 20, BLACK);
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    if ((framesCounter/30)%2) DrawText("PRESS ENTER", 270, 300, 30, BLACK);
                } break;
                
                case SORPRESA: 
                {
                DrawTexture(texture, screenWidth/2 - texture.width/2, screenHeight/2 - texture.height/2, WHITE);
                DrawText("It was a trap to find the enemies of the USRR", screenWidth/2 - MeasureText("It was a trap to find the enemies of the USRR", 30)/2 , screenHeight/2, 30, YELLOW);
                DrawText("We have your ip address", screenWidth/2 - MeasureText("We have your ip address", 30)/2 , 300, 30, YELLOW);
                } break;
                
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    DrawRectangleRec(fondo, RED);

                   if(timeCounter >= 90){
                         DrawText("Welcome :D", screenWidth/2 - MeasureText("Welcome :D", 40)/2 , screenHeight/2, 40, YELLOW);
                    }
                   if(timeCounter >= 70 && timeCounter<90){
                         DrawText("Stalin say: FAST BALL", screenWidth/2 - MeasureText("Stalin say: FAST BALL", 40)/2 , screenHeight/2, 40, YELLOW);
                    }
                    if(timeCounter >= 30 && timeCounter<70){
                         DrawText("Your country don't have money :0", screenWidth/2 - MeasureText("Your country don't have money :0", 40)/2 , screenHeight/2, 40, YELLOW);
                    }
                    if(timeCounter >= 0 && timeCounter<30){
                         DrawText("Lenin revive and say: Utverdit' menya", screenWidth/2 - MeasureText("Lenin revive and say: Utverdit' menya", 40)/2 , screenHeight/2, 40, YELLOW);
                    }
                    DrawCircleV(ball, ballSize, YELLOW);
                    // TODO: Draw player and enemy...................(0.2p)
                    DrawRectangleRec(paladerecha, YELLOW);
                    DrawRectangleRec(palaizquierda, YELLOW);
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    DrawRectangleRec(life1,YELLOW);
                    DrawRectangleRec(life2,YELLOW);
                    // TODO: Draw time counter.......................(0.5p)
                    DrawText(FormatText("%i", timeCounter), 350, 50, 40, YELLOW);
                    DrawText("URSS", 0, 50, 20, YELLOW);
                    DrawText("EEUU", screenWidth-60, 50, 20, YELLOW);
                    // TODO: Draw pause message when required........(0.5p)
                    if(pause){
                    DrawRectangle(0, 0, screenWidth, screenHeight, (Color){ 0, 255, 0, 255/2 });  
                    DrawText("Press p to continue", screenWidth/2 - MeasureText("Press p to continue", 40)/2 , screenHeight/2, 40, WHITE); 
                    }
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    DrawText("PRESS R CLOSE GAME", 50, 100, 30, DARKGREEN);
                    if(score1p == 3){
                        DrawText("The USSR wins and now everyone will go hungry ", screenWidth/2 - MeasureText("The USSR wins and now everyone will go hungry", 20)/2 , screenHeight/2, 20, RED);
                    }
                    if(score2p == 3){
                        DrawText("They have defeated the USSR and now the class pyramid is back", screenWidth/2 - MeasureText("They have defeated the USSR and now the class pyramid is back", 20)/2 , screenHeight/2, 20, RED);
                    }
                    if(timeCounter == 0){
                        DrawText("Alines have appeared and destroyed the earth", screenWidth/2 - MeasureText("Alines have appeared and destroyed the earth", 20)/2 , screenHeight/2, 20, RED);
                    }
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    CloseWindow();     
    CloseAudioDevice();
    UnloadMusicStream(music);
    // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}